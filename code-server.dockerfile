FROM codercom/code-server:3.0.0

# extensions to code-server
RUN code-server --install-extension ms-python.python && \
    code-server --install-extension vscjava.vscode-maven && \
    code-server --install-extension redhat.java

# ubuntu installations
RUN sudo -E apt-get update && \
    sudo -E apt-get install -y python3.7 python3-pip && \
    sudo apt-get install -y openjdk-11-jdk && \
    sudo apt-get install -y maven && \
    sudo apt-get install -y wget && \
    sudo rm -rf /var/lib/apt/lists/*

# Redis cli
RUN wget http://download.redis.io/releases/redis-5.0.7.tar.gz && \
      mv redis-5.0.7.tar.gz /tmp && \
      cd /tmp && \
      tar xzf redis-5.0.7.tar.gz && \
      cd redis-5.0.7 && \
      make && \
      echo 'alias redis-cli="/tmp/redis-5.0.7/src/redis-cli -h redis"' >> ~/.bashrc

# Maven
RUN sudo -E apt-get update && \
    sudo apt-get install -y maven && \
    sudo rm -rf /var/lib/apt/lists/*

# Remove sudo access
RUN export SUDO_FORCE_REMOVE=yes && sudo -E apt-get remove -y sudo

CMD ["code-server"]
