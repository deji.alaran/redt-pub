package com.redislabs.cacheaside.repositories;

import java.util.List;

import com.redislabs.cacheaside.dao.redis.RedisCustomer;

import org.springframework.data.repository.CrudRepository;

public interface RedisCustomerRepository extends CrudRepository<RedisCustomer, Long> {
 
    RedisCustomer findByExternalid(String externalId);
    List<RedisCustomer> findByAccountsId(Long id);
 
}