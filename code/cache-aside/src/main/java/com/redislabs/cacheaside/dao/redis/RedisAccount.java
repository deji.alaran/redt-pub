package com.redislabs.cacheaside.dao.redis;

import org.springframework.data.redis.core.index.Indexed;

public class RedisAccount {

    @Indexed
    private Long id;
    private String balance;
    @Indexed
    private int customerid;

    public RedisAccount(Long id, String balance, int customerid) {
        this.id = id;
        this.balance = balance;
        this.customerid = customerid;
    }
 
    public Long getId() {
        return id;
    }
 
    public void setId(Long id) {
        this.id = id;
    }
 
    public String getBalance() {
        return balance;
    }
 
    public void setBalance(String balance) {
        this.balance = balance;
    }

    public int getCustomerid() {
        return customerid;
    }

    public void setCustomerid(int customerid) {
        this.customerid = customerid;
    }
 
}