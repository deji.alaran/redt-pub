package com.redislabs.cacheaside.dao.redis;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

@RedisHash(value = "customer", timeToLive = 300)
public class RedisCustomer {

    @Id
    private Long id;
    @Indexed
    private String externalid;
    private String name;
    private List<RedisAccount> accounts = new ArrayList<>();

    public RedisCustomer(Long id, String externalid, String name) {
        this.id = id;
        this.externalid = externalid;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExternalid() {
        return externalid;
    }

    public void setExternalid(String externalId) {
        this.externalid = externalId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<RedisAccount> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<RedisAccount> accounts) {
        this.accounts = accounts;
    }

    public void addAccount(RedisAccount account) {
        this.accounts.add(account);
    }

    public String toString() {
        return "Customer " + getName();
    }
}