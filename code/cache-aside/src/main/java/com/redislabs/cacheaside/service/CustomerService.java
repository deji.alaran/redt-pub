package com.redislabs.cacheaside.service;

import java.util.Optional;
import java.util.Properties;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import com.redislabs.cacheaside.dao.rdbms.Account;
import com.redislabs.cacheaside.dao.rdbms.Customer;
import com.redislabs.cacheaside.dao.redis.RedisAccount;
import com.redislabs.cacheaside.dao.redis.RedisCustomer;
import com.redislabs.cacheaside.repositories.CustomerRepository;
import com.redislabs.cacheaside.repositories.RedisCustomerRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {

    Random random = new Random();
    public static boolean DO_CACHE_ASIDE = false;

    public final boolean doRandomError = false;

    private RedisCustomerRepository redisRepo;

    private CustomerRepository rdbmsRepo;

    @Autowired
    private RedisConnectionFactory connectionFactory;

    public CustomerService(RedisCustomerRepository redisRepo, CustomerRepository rdbmsRepo) {
        this.redisRepo = redisRepo;
        this.rdbmsRepo = rdbmsRepo;

        TimerTask task = new TimerTask() {
            int lastMiss = 0;
            int lastHit = 0;
            int lastExpired = 0;

            public void run() {
                Properties props = connectionFactory.getConnection().info();
                int misses = Integer.parseInt(props.get("keyspace_misses").toString());
                int hits = Integer.parseInt(props.get("keyspace_hits").toString());
                int expired = Integer.parseInt(props.get("expired_keys").toString());

                double missPerc = Math.round((misses - lastMiss) * 100.0 / (lastMiss == 0 ? 1 : lastMiss)) * 100.0
                        / 100.0;
                double hitPerc = Math.round((hits - lastHit) * 100.0 / (lastHit == 0 ? 1 : lastHit)) * 100.0 / 100.0;
                double expiredPerc = Math.round((expired - lastExpired) * 100.0 / (lastExpired == 0 ? 1 : lastExpired))
                        * 100.0 / 100.0;

                System.out.print("Misses " + misses + " (" + missPerc + "%) Hits " + hits + " (" + hitPerc
                        + "%) Expired " + expired + " (" + expiredPerc + "%)\r");

                lastMiss = misses;
                lastHit = hits;
                lastExpired = expired;
            }
        };
        Timer timer = new Timer("Timer");

        long delay = 1000L;
        timer.schedule(task, delay, 2000L);

    }

    public RedisCustomer getCustomer(String externalId) {
        /* TODO */
        // If we're using cache
        // - attempt to retrieve from redis repository
        // - if found return
        // - otherwise retrieve from rdbms, store, then return
        // Otherwise retrieve from rdbms and return

        return null;
    }

    public void clear() {
        connectionFactory.getConnection().flushDb();
        connectionFactory.getConnection().resetConfigStats();
    }
}