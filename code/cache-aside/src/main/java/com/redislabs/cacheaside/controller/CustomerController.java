package com.redislabs.cacheaside.controller;

import com.redislabs.cacheaside.dao.redis.RedisCustomer;
import com.redislabs.cacheaside.service.CustomerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping("/{id}")
    public RedisCustomer findCustomerByExternalId(@PathVariable String id) {
        return getCustomer(id);
    }

    private RedisCustomer getCustomer(String externalId) {
        return customerService.getCustomer(externalId);
    }
}