package com.redislabs.cacheaside.repositories;

import java.util.List;

import com.redislabs.cacheaside.dao.rdbms.Customer;

import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Long> {
 
    Customer findByExternalid(String externalId);
    List<Customer> findByAccountsId(Long id);
 
}