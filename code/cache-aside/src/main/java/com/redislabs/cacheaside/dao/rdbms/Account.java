package com.redislabs.cacheaside.dao.rdbms;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "account")
public class Account {

    @Id
    private Long id;
    private String balance;
    private int customerid;

    public Account() {

    }

    public Account(Long id, String balance, int customerid) {
        this.id = id;
        this.balance = balance;
        this.customerid = customerid;
    }
 
    public Long getId() {
        return id;
    }
 
    public void setId(Long id) {
        this.id = id;
    }
 
    public String getBalance() {
        return balance;
    }
 
    public void setBalance(String balance) {
        this.balance = balance;
    }

    public void setCustomerid(int customerid) {
        this.customerid = customerid;
    }

    public int getCustomerid() {
        return customerid;
    }
}