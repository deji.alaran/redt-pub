package com.redislabs.cacheaside.dao.rdbms;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "customer")
public class Customer {

    @Id
    private Long id;
    private String externalid;
    private String customername;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "customerid")
    private List<Account> accounts;

    public Customer() {

    }

    public Customer(Long id, String externalId, String name) {
        this.id = id;
        this.externalid = externalId;
        this.customername = name;
    }
    
    public Long getId() {
        return id;
    }
 
    public void setId(Long id) {
        this.id = id;
    }
 
    public String getExternalid() {
        return externalid;
    }
 
    public void setExternalid(String externalId) {
        this.externalid = externalId;
    }
 
    public String getCustomername() {
        return customername;
    }
 
    public void setCustomername(String name) {
        this.customername = name;
    }
 
    public List<Account> getAccounts() {
        return accounts;
    }
 
    public String toString() {
        return "Customer " + getCustomername();
    }

}