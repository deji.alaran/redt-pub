package com.redislabs.cacheaside.controller;
import java.util.List;

import com.redislabs.cacheaside.dao.redis.RedisCustomer;
import com.redislabs.cacheaside.repositories.RedisCustomerRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/accounts")
public class AccountsController {

    @Autowired
	private RedisCustomerRepository redisRepo;

    @GetMapping("/{id}")
    public List<RedisCustomer> findCustomerByAccountId(@PathVariable Long id) {
        return redisRepo.findByAccountsId(id);
    }

}