package com.redislabs.cacheaside.controller;

import com.redislabs.cacheaside.service.CustomerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/admin")
public class AdminController {

    @Autowired
    private CustomerService customerService;

    @GetMapping("/cache/{mode}")
    public Boolean doCacheMode(@PathVariable String mode) {
        CustomerService.DO_CACHE_ASIDE = "on".equals(mode) ? true : false;
        if (!CustomerService.DO_CACHE_ASIDE) {
            customerService.clear();
        }
        return CustomerService.DO_CACHE_ASIDE;
    }

}