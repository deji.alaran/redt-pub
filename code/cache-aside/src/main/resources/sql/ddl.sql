CREATE TABLE customer (
    id SERIAL PRIMARY KEY,
    externalid CHARACTER(10),
    customername CHARACTER(50)
);

CREATE TABLE account (
    id SERIAL PRIMARY KEY,
    customerid INT REFERENCES customer(id),
    balance MONEY
);

CREATE INDEX  customer_externalid_idx ON customer 
USING btree
(
    externalid ASC NULLS LAST
);