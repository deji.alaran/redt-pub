package com.redislabs.developertraining;

import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;

@Ignore
public class GeoLabTest extends LabTestBase {
    private final GeoLab geoLab = new GeoLab(jedis);
    private String GEO_KEY;

    private final Landmark empireState = new Landmark(
        "Empire State Building",
        -73.9878584,
        40.7484405
    );

    private final Landmark flatiron = new Landmark(
        "Flatiron Building",
        -73.9918926,
        40.7410605
    );

    private final Landmark madisonSqGdn = new Landmark(
        "Madison Square Garden",
        -73.9956327,
        40.7505045
    );

    private final Landmark timesSquare = new Landmark(
        "Times Square",
        -73.9877366,
        40.7579747
    );

    public GeoLabTest () {
        GEO_KEY = getKeyName("labs:geo");
    }

    @After
    public void afterTest() {
        deleteKeys();
    }

    @Test
    public void addLandmarkTest() {
        geoLab.addLandmark(GEO_KEY, empireState);

        Long geoKeyEntries = jedis.zcard(GEO_KEY);
        Long rank = jedis.zrank(GEO_KEY, empireState.getName());
        Double geohash = jedis.zscore(GEO_KEY, empireState.getName());

        assertThat(geoKeyEntries, is(1L));
        assertThat(rank, is(0L));
        assertThat(geohash, is(1791875671103679D));
    }

    @Test
    public void getLandmarksByRadiusTest() {
        setupLandmarks();

        double longitude = -73.9946227;
        double latitude = 40.7497452;
        double radius = 0.6;

        List<LandmarkWithDistance> landmarks = geoLab.getLandmarksByRadius(GEO_KEY, longitude, latitude, radius);

        assertThat(landmarks.size(), is(2));

        LandmarkWithDistance lmd1 = landmarks.get(0);
        assertThat(lmd1.getName(), is(empireState.getName()));
        assertThat(lmd1.getLatitude(), greaterThan(0D));
        assertThat(lmd1.getLongitude(), lessThan(0D));
        assertThat(lmd1.getDistance(), is(0.3655));

        LandmarkWithDistance lmd2 = landmarks.get(1);
        assertThat(lmd2.getName(), is(madisonSqGdn.getName()));
        assertThat(lmd2.getLatitude(), greaterThan(0D));
        assertThat(lmd2.getLongitude(), lessThan(0D));
        assertThat(lmd2.getDistance(), is(0.0745));
    }

    @Test
    public void getDistanceBetweenLandmarksTest() {
        setupLandmarks();

        double distance = geoLab.getDistanceBetweenLandmarks(
            GEO_KEY,
            timesSquare.getName(),
            flatiron.getName()
        );

        assertThat(distance, is(1.1891));
    }

    @Test
    public void getLandmarkNamesTest() {
        setupLandmarks();

        Set<String> landmarkNames = geoLab.getLandmarkNames(GEO_KEY);

        assertThat(landmarkNames.size(), is(4));
        assertThat(landmarkNames.contains(empireState.getName()), is(true));
        assertThat(landmarkNames.contains(flatiron.getName()), is (true));
        assertThat(landmarkNames.contains(madisonSqGdn.getName()), is (true));
        assertThat(landmarkNames.contains(timesSquare.getName()), is (true));
    }

    private void setupLandmarks() {
        jedis.geoadd(GEO_KEY, empireState.getLongitude(), empireState.getLatitude(), empireState.getName());
        jedis.geoadd(GEO_KEY, flatiron.getLongitude(), flatiron.getLatitude(), flatiron.getName());
        jedis.geoadd(GEO_KEY, madisonSqGdn.getLongitude(), madisonSqGdn.getLatitude(), madisonSqGdn.getName());
        jedis.geoadd(GEO_KEY, timesSquare.getLongitude(), timesSquare.getLatitude(), timesSquare.getName());
    }
}
