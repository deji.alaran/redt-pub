package com.redislabs.developertraining;

import redis.clients.jedis.*;

import java.util.Set;

public class LabTestBase {
    private final static String TEST_KEY_PREFIX = "java:test";

    protected static Jedis jedis;

    public LabTestBase() {
        jedis = RedisConnection.getConnection();
    }

    protected String getKeyName(String keyName) {
        return TEST_KEY_PREFIX + ":" + keyName;
    }

    protected void deleteKeys() {
        Set<String> keysToDelete = jedis.keys(TEST_KEY_PREFIX + ":*");
        for (String key : keysToDelete) {
            jedis.del(key);
        }
    }

    protected void flushdb() {
        jedis.flushDB();
    }
}
