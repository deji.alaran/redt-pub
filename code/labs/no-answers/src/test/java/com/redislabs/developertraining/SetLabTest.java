package com.redislabs.developertraining;

import org.junit.After;
import org.junit.Ignore;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Ignore
public class SetLabTest extends LabTestBase {
    private final SetLab setLab = new SetLab(jedis);
    private String SET_1_KEY;
    private String SET_2_KEY;
    private String SET_3_KEY;

    public SetLabTest() {
        SET_1_KEY = getKeyName("labs:set:1");
        SET_2_KEY = getKeyName("labs:set:2");
        SET_3_KEY = getKeyName("labs:set:3");
    }

    @After
    public void afterTest() {
        deleteKeys();
    }

    @Test
    public void getIntersectingCount() {
        jedis.sadd(SET_1_KEY, "a", "b", "c", "d", "e", "f", "g");
        jedis.sadd(SET_2_KEY, "a", "h", "d", "i", "e", "j", "k");

        int intersectingCount = setLab.getIntersectingCount(SET_1_KEY, SET_2_KEY);
        assertThat(intersectingCount, is(3));
    }

    @Test
    public void getSetDifferenceAndStore() {
        jedis.sadd(SET_1_KEY, "a", "b", "c", "d", "e", "f", "g");
        jedis.sadd(SET_2_KEY, "a", "h", "i", "c", "j", "g", "k");
        jedis.del(SET_3_KEY);
        setLab.getSetDifferenceAndStore(SET_1_KEY, SET_2_KEY, SET_3_KEY);

        Set<String> resultSet = jedis.smembers(SET_3_KEY);
        assertThat(resultSet.size(), is(4));
        assertThat(resultSet.contains("b"), is(true));
        assertThat(resultSet.contains("d"), is(true));
        assertThat(resultSet.contains("e"), is(true));
        assertThat(resultSet.contains("f"), is(true));
    }

    @Test
    public void addUserWithTagsTest() {
        Map<String, String> employee1Props = new HashMap<>();
        employee1Props.put("id", "2707");
        employee1Props.put("firstname", "Sandrine");
        employee1Props.put("lastname", "Berger");
        employee1Props.put("yearsexperience", "12");
        employee1Props.put("vacationdays", "10");
        employee1Props.put("tags", "superstar,moderator,editor,staff");

        Map<String, String> employee2Props = new HashMap<>();
        employee2Props.put("id", "2708");
        employee2Props.put("firstname", "Robin");
        employee2Props.put("lastname", "Johnson");
        employee2Props.put("yearsexperience", "8");
        employee2Props.put("vacationdays", "10");
        employee2Props.put("tags", "moderator,veteran,mentor,admin");

        Map<String, String> employee3Props = new HashMap<>();
        employee3Props.put("id", "2709");
        employee3Props.put("firstname", "Brendon");
        employee3Props.put("lastname", "Mendoza");
        employee3Props.put("yearsexperience", "3");
        employee3Props.put("vacationdays", "8");
        employee3Props.put("tags", "editor,staff,moderator,admin");

        Map<String, String> employee4Props = new HashMap<>();
        employee4Props.put("id", "2710");
        employee4Props.put("firstname", "Madison");
        employee4Props.put("lastname", "Hoffman");
        employee4Props.put("yearsexperience", "1");
        employee4Props.put("vacationdays", "8");
        employee4Props.put("tags", "mentor,newbie,staff");

        setLab.addUserWithTags(getKeyName("labs:set:users" + employee1Props.get("id")), employee1Props);
        setLab.addUserWithTags(getKeyName("labs:set:users" + employee2Props.get("id")), employee2Props);
        setLab.addUserWithTags(getKeyName("labs:set:users" + employee3Props.get("id")), employee3Props);
        setLab.addUserWithTags(getKeyName("labs:set:users" + employee4Props.get("id")), employee4Props);

        Set<String> employeeIds = setLab.getEmployeeIdsWithTags("staff", "moderator", "editor");
        assertThat(employeeIds.size(), is(2));
        assertThat(employeeIds.contains("2707"), is(true));
        assertThat(employeeIds.contains("2709"), is(true));

        employeeIds = setLab.getEmployeeIdsWithTags("newbie", "admin");
        assertThat(employeeIds.size(), is(0));

        employeeIds = setLab.getEmployeeIdsWithTags("newbie");
        assertThat(employeeIds.size(), is(1));
        assertThat(employeeIds.contains("2710"), is(true));
    }
}
