package com.redislabs.developertraining;

import org.junit.After;
import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Ignore
public class PipeliningLabTest extends LabTestBase {
    private final PipeliningLab pipeliningLab = new PipeliningLab(jedis);
    private final String KEY_BASE = getKeyName("labs:pipelining");
    private final int NUM_COMMANDS = 1000000;

    @After
    public void afterTest() {
        flushdb();
    }

    @Test
    public void withoutPipeline() {
        pipeliningLab.setNoPipeline(KEY_BASE, NUM_COMMANDS);
    }

    @Test
    public void withPipeline() {
        pipeliningLab.setWithPipeline(KEY_BASE, NUM_COMMANDS);
    }

    @Test
    public void getResultsFromPipeline() {
        Long result = pipeliningLab.getResultsFromPipeline(getKeyName("labs:pipelining:results"));
        assertThat(result, is(6L));
    }
}
