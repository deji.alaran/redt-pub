package com.redislabs.developertraining;

import org.junit.After;
import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Ignore
public class BitFieldLabTest extends LabTestBase {
    private final BitFieldLab bitFieldLab = new BitFieldLab(jedis);

    @After
    public void afterTest() {
        deleteKeys();
    }

    @Test
    public void setBitTest() {
        String key = getKeyName("labs:bitfield:setbit");
        bitFieldLab.setBit(key, 0, true);
        bitFieldLab.setBit(key, 3, true);

        // Check number of bits set is correct.
        assertThat(jedis.bitcount(key), is(2L));

        // Check bits 0 and 3 are set.
        assertThat(jedis.getbit(key, 0), is(true));
        assertThat(jedis.getbit(key, 3), is(true));

        // Check some other bits are not set.
        assertThat(jedis.getbit(key, 1), is(false));
        assertThat(jedis.getbit(key, 2), is(false));
    }

    @Test
    public void checkUserPermissionsTest() {
        // User 1 has all permissions and can access the route.
        String user1Key = getKeyName("permissions:users:1");
        jedis.setbit(user1Key, 0, true);
        jedis.setbit(user1Key, 1, true);
        jedis.setbit(user1Key, 2, true);
        jedis.setbit(user1Key, 3, true);

        // User 2 has some permissions and can access the route.
        String user2Key = getKeyName("permissions:users:2");
        jedis.setbit(user2Key, 0, true);
        jedis.setbit(user2Key, 1, true);
        jedis.setbit(user2Key, 2, false);
        jedis.setbit(user2Key, 3, true);

        // User 3 has some permissions and can't access the route.
        String user3Key = getKeyName("permissions:users:3");
        jedis.setbit(user3Key, 0, true);
        jedis.setbit(user3Key, 1, true);
        jedis.setbit(user3Key, 2, true);
        jedis.setbit(user3Key, 3, false);

        // User 4 has no permissions - shouldn't need
        // to set these bits but let's do it anyway.
        String user4Key = getKeyName("permissions:users:4");
        jedis.setbit(user4Key, 0, false);
        jedis.setbit(user4Key, 1, false);
        jedis.setbit(user4Key, 2, false);
        jedis.setbit(user4Key, 3, false);

        // Our test route requires users to have the permissions
        // identified by bits at offset 0 and 3
        String routeKey = getKeyName("permissions:routes:/accounts");
        jedis.setbit(routeKey, 0, true);
        jedis.setbit(routeKey, 3, true);

        // User 1 should be granted access to the route.
        assertThat(bitFieldLab.checkUserPermissions(user1Key, routeKey), is(true));

        // User 2 should be granted access to the route.
        assertThat(bitFieldLab.checkUserPermissions(user2Key, routeKey), is(true));

        // User 3 should not be granted access to the route.
        assertThat(bitFieldLab.checkUserPermissions(user3Key, routeKey), is(false));

        // User 4 should not be granted access to the route.
        assertThat(bitFieldLab.checkUserPermissions(user4Key, routeKey), is(false));
    }
}
