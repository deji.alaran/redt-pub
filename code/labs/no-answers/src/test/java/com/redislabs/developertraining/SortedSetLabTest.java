package com.redislabs.developertraining;

import org.junit.After;
import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Ignore
public class SortedSetLabTest extends LabTestBase {
    private final SortedSetLab sortedSetLab = new SortedSetLab(jedis);

    @After
    public void afterTest() {
        //deleteKeys();
    }

    @Test
    public void vacationDaysIndexTest() {
        Map<String, String> employee1Props = new HashMap<>();
        employee1Props.put("id", "2707");
        employee1Props.put("firstname", "Sandrine");
        employee1Props.put("lastname", "Berger");
        employee1Props.put("yearjoined", "2006");
        employee1Props.put("vacationdays", "12");

        Map<String, String> employee2Props = new HashMap<>();
        employee2Props.put("id", "2708");
        employee2Props.put("firstname", "Robin");
        employee2Props.put("lastname", "Johnson");
        employee2Props.put("yearjoined", "2008");
        employee2Props.put("vacationdays", "8");

        Map<String, String> employee3Props = new HashMap<>();
        employee3Props.put("id", "2709");
        employee3Props.put("firstname", "Brendon");
        employee3Props.put("lastname", "Mendoza");
        employee3Props.put("yearjoined", "2018");
        employee3Props.put("vacationdays", "3");

        Map<String, String> employee4Props = new HashMap<>();
        employee4Props.put("id", "2710");
        employee4Props.put("firstname", "Madison");
        employee4Props.put("lastname", "Hoffman");
        employee4Props.put("yearjoined", "2020");
        employee4Props.put("vacationdays", "1");

        sortedSetLab.addEmployee(employee1Props);
        sortedSetLab.addEmployee(employee2Props);
        sortedSetLab.addEmployee(employee3Props);
        sortedSetLab.addEmployee(employee4Props);

        List<String> results = sortedSetLab.getEmployeesByVacationDays(1, 99, true);
        assertThat(results.size(), is(4));
        assertThat(results.get(0), is("1.0 Madison Hoffman"));
        assertThat(results.get(1), is("3.0 Brendon Mendoza"));
        assertThat(results.get(2), is("8.0 Robin Johnson"));
        assertThat(results.get(3), is("12.0 Sandrine Berger"));

        results = sortedSetLab.getEmployeesByVacationDays(1, 99, false);
        assertThat(results.size(), is(4));
        assertThat(results.get(0), is("12.0 Sandrine Berger"));
        assertThat(results.get(1), is("8.0 Robin Johnson"));
        assertThat(results.get(2), is("3.0 Brendon Mendoza"));
        assertThat(results.get(3), is("1.0 Madison Hoffman"));
    }
}
