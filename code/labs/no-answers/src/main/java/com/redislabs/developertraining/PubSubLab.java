package com.redislabs.developertraining;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

import java.util.Random;

public class PubSubLab {
    private static Jedis jedis = RedisConnection.getConnection();
    private static String CHANNEL_BASE = "weather";

    private static String []cities = { "singapore", "frankfurt", "madrid", "london" };
    private static String []conditions = { "Cloudy", "Overcast", "Sunny", "Rain", "Thunderstorms", "Clear" };

    private static String getCity() {
        Random r = new Random();
        return cities[r.nextInt(cities.length)];
    }

    private static String getWeather() {
        Random r = new Random();
        String weatherSummary = conditions[r.nextInt(conditions.length)];
        return (r.nextInt(65) + 28) + "F " + weatherSummary;
    }

    private static void publisher() {
        // BEGIN CODE CHALLENGE

        // TODO

        // END CODE CHALLENGE
    }

    private static void subscriber() {
        // BEGIN CODE CHALLENGE

        // TODO
        
        // END CODE CHALLENGE
    }

    private static void usage() {
        System.err.println("Usage: PubSubLab publisher|subscriber");
        System.exit(1);
    }

    public static void main(String[] args) {
        if (args.length == 1) {
            switch (args[0]) {
                case "publisher":
                    publisher();
                    break;

                case "subscriber":
                    subscriber();
                    break;

                default:
                    usage();
            }
        } else {
            usage();
        }
    }
}
