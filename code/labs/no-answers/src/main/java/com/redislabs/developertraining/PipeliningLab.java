package com.redislabs.developertraining;

import java.util.Date;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Response;

public class PipeliningLab extends LabBase {
    public PipeliningLab(Jedis jedis) { super(jedis); }

    /**
     * Without using a pipeline, send a number of SET commands to
     * Redis, each setting a different key to a different value.
     *
     * Use keyBase as the base for the key name, and howMany as
     * the number of commands to send.
     *
     * The time taken to send all of the commands to Redis will
     * be calculated for you and output to the console.
     *
     * No coding required here, this method is provided in order
     * to compare the run time with that of the "setWithPipeline"
     * method that you will need to write yourself.
     *
     * @param keyBase The base key name to use when creating new keys in Redis.
     * @param howMany Number of commands to send to Redis.
     */
    public void setNoPipeline(String keyBase, int howMany) {
        Date startDate = new Date();

        for (int n = 0; n < howMany; n++) {
            String keyName = keyBase + ":" + n;
            jedis.set(keyName, String.valueOf(n));
        }

        Date endDate = new Date();
        long elapsedTime = endDate.getTime() - startDate.getTime();

        System.out.println("No pipeline, elapsed time " + elapsedTime + " milliseconds, " + howMany + " commands sent.");
    }

    /**
     * Using the "setNoPipeline" method above as an example, re-implement that
     * method to send all of the SET commands to Redis in a single pipeline.
     *
     * There is no need to get any results back from Redis.
     *
     * @param keyBase The base key name to use when creating new keys in Redis.
     * @param howMany Number of commands to include in the pipeline.
     */
    public void setWithPipeline(String keyBase, int howMany) {
        Pipeline pipeline = jedis.pipelined();

        Date startDate = new Date();

        // START CODING CHALLENGE

        // TODO

        // END CODING CHALLENGE

        Date endDate = new Date();
        long elapsedTime = endDate.getTime() - startDate.getTime();

        System.out.println("With pipeline, elapsed time " + elapsedTime + " milliseconds, " + howMany + " commands sent.");
    }

    /**
     * Using a pipeline, perform a series of Redis commands against a
     * supplied key and return the value held in that key after all
     * of the pipelined commands have been executed.
     *
     * @param keyName The Redis key to use.
     * @return Value held in keyName at the end of the operation.
     */
    public Long getResultsFromPipeline(String keyName) {
        Pipeline pipeline = jedis.pipelined();

        // Use "pipeline" to send the following commands to Redis:
        //
        // * SET keyName "1"
        // * INCR keyName
        // * INCRBY keyName 7
        // * INCRBY keyName -3
        //
        // The INCRBY command returns the new value stored in the key
        // that was modified, and Jedis returns this as a Java Long.
        //
        // Capture the value from the last command (INCRBY keyName -3)
        // and return it to the caller.

        // START CODING CHALLENGE

        return -1L;

        // END CODING CHALLENGE
    }
}
