package com.redislabs.developertraining;

import redis.clients.jedis.Jedis;

public class StringLab extends LabBase {
    public StringLab(Jedis jedis) {
        super(jedis);
    }

    /**
     * Sets the value stored at key to be value.
     * @param key Redis key.
     * @param value Value to be stored.
     */
    public void setString(String key, String value) {
        // TODO
    }

    /**
     * Gets the value stored at key.
     * @param key Redis key.
     * @return The value stored at key.
     */
    public String getString(String key) {
        // TODO
        return "";
    }

    /**
     * Sets an expiry time for a key.
     * @param key Redis key.
     * @param expiry Time in seconds before the key expires.
     */
    public void setExpiry(String key, int expiry) {
        // TODO
    }

    /**
     * Gets the remaining time to live for a key.
     * @param key Redis key.
     * @return
     */
    public Long getTimeToLive(String key) {
        // TODO
        return -2L;
    }

    /**
     * Returns the last 5 characters from the string
     * value stored at key.
     * @param key Redis key.
     * @return String containing last 5 characters from the value at key.
     */
    public String getStringRange(String key) {
        // TODO
        return "";
    }
}
