package com.redislabs.developertraining;

import redis.clients.jedis.BitOP;
import redis.clients.jedis.Jedis;

public class BitFieldLab extends LabBase {
    public BitFieldLab(Jedis jedis) {
        super(jedis);
    }

    /**
     * Sets a specific bit in a key's value.
     *
     * @param key Redis key to be operated on.
     * @param offset Offset of the bit to set.
     * @param value value to set the bit to.
     */
    public void setBit(String key, long offset, boolean value) {
       // TODO 
    }

    /**
     * Check whether the user has permissions to access a route.
     * Implemented using Redis bitfields.
     *
     * @param userKey Redis key containing user permissions.
     * @param routeKey Redis key containing route permissions.
     * @return true if userKey has the right permissions to access routeKey, false otherwise.
     */
    public boolean checkUserPermissions(String userKey, String routeKey) {
        // Use a temporary key name to store intermediate results.
        // Question: what problems might you encounter using a fixed
        //           key name here and how might those be addressed?
        String tmpKey = "tmpkey";

        // START CODING CHALLENGE

        // Algorithm:
        //
        // 1. Perform a bit operation to AND the values in userKey
        //    and routeKey, storing the result in tmpKey.
        //
        // 2. Perform a bit operation to XOR the values in routeKey
        //    and tmpKey, storing the result in tmpKey.
        //
        // 3. Count the number of bits set in tmpKey.
        //
        // 4. Delete tmpKey.
        //
        // 5. If the number of bits set in tmpKey was 0, return true
        //    otherwise return false.

        // TODO perform the AND operation
        // TODO perform the XOR operation

        long bitsSet = jedis.bitcount(tmpKey);
        jedis.unlink(tmpKey);
        return (bitsSet == 0);

        // END CODING CHALLENGE
    }
}
