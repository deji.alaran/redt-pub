package com.redislabs.developertraining;

import java.util.Random;

import redis.clients.jedis.Jedis;

public class HyperLogLogLab {
    private static Jedis jedis = RedisConnection.getConnection();

    private final static String HYPERLOGLOG_KEY = "hyperlogloglab:hll";
    private final static String SET_KEY = "hyperlogloglab:set";

    private static String generateRandomHexString() {
        Random r = new Random();
        String hex = Integer.toHexString(r.nextInt());

        while (hex.length() < 8) {
            hex = "0" + hex;
        }

        return hex.substring(0, 4);
    }

    /**
     * Generate a random IP v6 style string, format 8 x hex
     * blocks, colon separated:
     *
     * 0000:0000:0000:0000:0000:0000:0000:0000
     *
     * @return IP v6 style string.
     */
    private static String generateRandomIPv6() {
        StringBuffer buf = new StringBuffer();

        for (int n = 0; n < 8; n++) {
            if (n > 0) {
                buf.append(":");
            }

            buf.append(generateRandomHexString());
        }

        return buf.toString();
    }

    private static void runLab() {
        // Delete keys to start fresh.
        jedis.del(HYPERLOGLOG_KEY);
        jedis.del(SET_KEY);

        // Loop generating lots of values
        // Should probably do this in a pipeline...
        // or using variadic sadd / pfadd if available...

        // START CODE CHALLENGE.
        for (int n = 0; n < 1000000; n++) {
            // Add to both data structures.
            String ipAddr = generateRandomIPv6();

            // TODO add "ipAddr" to the set at "SET_KEY".
            // TODO add "ipAddr" to the hyperloglog at "HYPERLOGLOG_KEY"

            if (n % 1000 == 0) {
                System.out.print(".");
            }
        }

        System.out.println("");

        // TODO System.out.println the approximate cardinality of 
        // the hyperloglog at "HYPERLOGLOG_KEY".

        // TODO System.out.println the cardinality of the set at "SET_KEY".

        // END CODE CHALLENGE.
    }

    public static void main(String[] args) {
        runLab();
    }
}