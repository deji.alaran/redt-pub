package com.redislabs.developertraining;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Tuple;

import java.util.*;

public class SortedSetLab extends LabBase {
    public SortedSetLab(Jedis jedis) {
        super(jedis);
    }

    /**
     * This method is similar to one we saw in the Hash lab, so part of it
     * has been implemented for you.
     *
     * @param employeeProperties map of key/value pairs for the employee.
     */
    public void addEmployee(Map<String, String> employeeProperties) {
        // Write the employee hash to Redis.
        jedis.hmset("java:test:employees:" + employeeProperties.get("id"), employeeProperties);

        // To complete this method, you need to use the following
        // entries from the employeeProperties map:
        //
        // * id - the employee's ID.
        // * vacationdays - the number of vacation days that the employee has.
        //
        // You will need to store the employee's vacation days as the
        // score in a sorted set at key:
        //
        // java:test:employeesbyvacationdays
        //
        // use the employee's id as the value to store in the sorted set.

        String vacationDays = employeeProperties.get("vacationdays");
        String employeeId = employeeProperties.get("id");

        // TODO
    }

    /**
     * Return a list of employee number of vacation days and full names in order of
     * number of vacation days.
     *
     * @param minDays the minimum number of vacation days an employee should have.
     * @param maxDays the maximum number of years vacation days an employee should have.
     * @param ascending results will be returned in ascending order of vacation days
     *                  when this is true, otherwise in descending order.
     * @return List of strings formatted <vacation days> <firstname> <lastname>
     */
    public List<String> getEmployeesByVacationDays(double minDays, double maxDays, boolean ascending) {
        // To complete this method, you will need to:
        //
        // * Retrieve the employee IDs and vacation days scores from the
        //   sorted set at key java:test:employeesbyvacationdays.
        // * Make sure that you retrieve the sorted set members in
        //   ascending score order if ascending = true, or descending
        //   score order if ascending is false.
        // * Make sure to only retrieve employees whose scores are between
        //   the values of minDays and maxDays.
        // * Once you have the employee IDs and scores, get each employee's
        //   firstname and lastname from the hash at java:test:employees:<id>
        //   and use this to make a String containing:
        //
        //   <score> <firstname> <lastname>
        //
        //   Example:
        //
        //   10 Jane Doe
        //
        // * Add this string to the List of strings to return.
        // * Consider using the HMGET command to get just the fields that
        //   you need from the employee hash.

        Set<Tuple> idsWithScores = new HashSet<Tuple>();
        String sortedSetKey = "java:test:employeesbyvacationdays";

        if (ascending) {
            // TODO set idsWithScores to be the ascending range 
            // of values from "sortedSetKey" with scores between 
            // "minDays" and "maxDays".
        } else {
            // TODO set idsWithScores to be the descending range
            // of values from "sortedSetKey" with scores between
            // "maxDays" and "minDays".
        }

        List<String> results = new ArrayList<String>();

        for (Tuple t : idsWithScores) {
            List<String> employeeFields = jedis.hmget("java:test:employees:" + t.getElement(), "firstname", "lastname");

            String employeeResult = String.valueOf(t.getScore()) + " " + employeeFields.get(0) + " " + employeeFields.get(1);
            results.add(employeeResult);
        }

        return results;
    }
}
