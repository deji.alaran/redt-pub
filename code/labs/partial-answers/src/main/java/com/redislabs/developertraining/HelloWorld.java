package com.redislabs.developertraining;

// This is a basic connectivity test to make
// sure that the student environment is up
// and running.

import redis.clients.jedis.Jedis;

public class HelloWorld {
    public static void main(String[] args) {
        Jedis jedis = RedisConnection.getConnection();
        System.out.println(jedis.ping());
    }
}
