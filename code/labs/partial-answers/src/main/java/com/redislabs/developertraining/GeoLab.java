package com.redislabs.developertraining;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.GeoUnit;
import redis.clients.jedis.params.GeoRadiusParam;
import redis.clients.jedis.GeoRadiusResponse;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GeoLab extends LabBase {
    public GeoLab(Jedis jedis) {
        super(jedis);
    }

    /**
     * Add a new landmark named "landmark" to the Redis geospatial
     * index at "key".  "longitude" and "latitude" contain the
     * long/lat location of the new landmark.
     *
     * @param key Redis key that holds a geospatial index.
     * @param landmark Landmark to add.
     */
    public void addLandmark(String key, Landmark landmark) {
        // TODO
    }

    /**
     * Returns a List of LandmarkWithDistance objects containing details of
     * landmarks found within a specified radius in miles of a long/lat location.
     *
     * @param key Redis key that holds a geospatial index.
     * @param longitude Longitude of the point to search from.
     * @param latitude Latitude of the point to search from.
     * @param radius Radius to search, in miles.
     * @return List of landmarks with their distances from the center of the radius in miles.
     */
    public List<LandmarkWithDistance> getLandmarksByRadius(String key, double longitude, double latitude, double radius) {
        ArrayList<LandmarkWithDistance> result = new ArrayList<>();

        // START CODING CHALLENGE

        // TODO replace the following line with code to get 
        // landmarks by radius from Redis.
        List <GeoRadiusResponse> responses = new ArrayList<GeoRadiusResponse>();

        for (GeoRadiusResponse response : responses) {
            result.add(new LandmarkWithDistance(
                    response.getMemberByString(),
                    response.getCoordinate().getLongitude(),
                    response.getCoordinate().getLatitude(),
                    response.getDistance()
            ));
        }

        // END CODING CHALLENGE

        return result;
    }

    /**
     * Get the distance between two landmarks in miles.
     *
     * @param key Redis key that holds a geospatial index.
     * @param firstLandmark the first of the two landmarks.
     * @param secondLandmark the second of the two landmarks.
     * @return The distance between firstLandmark and secondLandmark.
     */
    public Double getDistanceBetweenLandmarks(String key, String firstLandmark, String secondLandmark) {
        return -1.0;
    }

    /**
     * Get just the names of all the landmarks in the Redis
     * geospatial index at "key".
     *
     * @param key Redis key that holds a geospatial index.
     * @return Set of strings containing landmark names.
     */
    public Set<String> getLandmarkNames(String key) {
        // TODO - hint, use a sorted set command here...
        return new HashSet<String>();
    }
}
