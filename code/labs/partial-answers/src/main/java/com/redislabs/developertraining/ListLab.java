package com.redislabs.developertraining;

import java.util.ArrayList;
import java.util.List;
import redis.clients.jedis.Jedis;

public class ListLab {
    private static Jedis jedis = RedisConnection.getConnection();

    private final static String KEY_NAME = "java:listlab:numbers";

    /**
     * Pushes items onto a list at a fixed rate of one per
     * second.  Stops after 20 items.
     */
    private static void producer() {
        System.out.println("Starting producer.");
        jedis.del(KEY_NAME);

        // Sets the 10th number to be the "priority" number.
        int priority = 10;

        System.out.println("Priority number is " + priority);

        // START CODING CHALLENGE
        // Push the numbers 1..20 onto the tail of the list at
        // KEY_NAME at a rate of one per second, then stop.
        //
        // One number has higher priority than the others, so when
        // pushing a number whose value matches that stored in the
        // variable "priority", it should go on the head of the
        // list rather than the tail.

        try {
            for (int n = 1; n < 21; n++) {
                Thread.sleep(1000);
                if (n == priority) {
                    System.out.println("Priority pushing " + n);
                    // TODO push the priority number onto the tail of the list.
                } else {
                    System.out.println("Pushing " + n);
                    // TODO push the number onto the head of the list.
                }
            }
        } catch (InterruptedException e) {
            System.err.println("Producer interrupted during sleep.");
        }

        // END CODING CHALLENGE

        System.out.println("Producer shutting down.");
    }

    /**
     * Pops items off of the list, blocking for up to
     * 2 seconds each time it accesses the list.  If
     * the list is empty 5 consecutive times, the
     * consumer stops trying and returns.
     */
    private static void consumer() {
        System.out.println("Starting consumer.");

        int retryCount = 0;

        while (retryCount < 5) {
            // START CODING CHALLENGE

            // Algorithm:
            //
            // * Perform a blocking right pop on the list at KEY_NAME
            // * Block for up to 2 seconds
            // * If the response is empty:
            //     * Increment retryCount
            // * If the response is not empty:
            //     * Set retryCount to 0
            //     * Log the response
            //     * What do you notice about the List<String> returned?

            // TODO replace the next line with code to perform a 
            // blocking right pop on the list waiting 2 seconds for responses.
            List<String> responses = new ArrayList<String>();

            if (responses == null) {
                System.out.println("List was empty.");
                retryCount++;
            } else {
                // We got a response.
                for (int n = 1; n < responses.size(); n += 2) {
                    System.out.println("Popped " + responses.get(n));
                }

                // Reset the retry count.
                retryCount = 0;
            }

            // END CODING CHALLENGE
        }

        System.out.println("Consumer shutting down.");
    }

    private static void usage() {
        System.err.println("Usage: ListLab producer|consumer");
        System.exit(1);
    }

    public static void main(String[] args) {
        if (args.length == 1) {
            switch (args[0]) {
                case "producer":
                    producer();
                    break;

                case "consumer":
                    consumer();
                    break;

                default:
                    usage();
            }
        } else {
            usage();
        }
    }
}
