package com.redislabs.developertraining;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

import java.util.Random;

public class PubSubLab {
    private static Jedis jedis = RedisConnection.getConnection();
    private static String CHANNEL_BASE = "weather";

    private static String []cities = { "singapore", "frankfurt", "madrid", "london" };
    private static String []conditions = { "Cloudy", "Overcast", "Sunny", "Rain", "Thunderstorms", "Clear" };

    private static String getCity() {
        Random r = new Random();
        return cities[r.nextInt(cities.length)];
    }

    private static String getWeather() {
        Random r = new Random();
        String weatherSummary = conditions[r.nextInt(conditions.length)];
        return (r.nextInt(65) + 28) + "F " + weatherSummary;
    }

    private static void publisher() {
        // BEGIN CODE CHALLENGE
        try {
            for (int n = 0; n < 20; n++) {
                String channelName = CHANNEL_BASE + ":" + getCity();
                String message = getWeather();

                // TODO publish "message" on "channelName" and record
                // number of recipients in "recipientCount"
                Long recipientCount = 0L;
                System.out.println("Channel: " + channelName + " Message: " + message + " sent to " + recipientCount + " subscribers.");

                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            System.err.println("Publisher interrupted during sleep.");
        }
        // END CODE CHALLENGE
    }

    private static void subscriber() {
        // BEGIN CODE CHALLENGE
        JedisPubSub pubSub = new JedisPubSub() {
            @Override
            public void onMessage(String channel, String message) {
                System.out.println("Channel: " + channel + " Message: " + message);
            }

            @Override
            public void onSubscribe(String channel, int subscribedChannels) {
                System.out.println("Subscribed to: " + channel);
            }

            @Override
            public void onPMessage(String pattern, String channel, String message) {
                System.out.println("Pattern: " + pattern + " Channel: " + channel + " Message: " + message);
            }

            @Override
            public void onPSubscribe(String pattern, int subscribedChannels) {
                System.out.println("Subscribed to: " + pattern);
            }
        };

        // TODO use "pubSub" to subscribe to channel(s) here.

        // END CODE CHALLENGE
    }

    private static void usage() {
        System.err.println("Usage: PubSubLab publisher|subscriber");
        System.exit(1);
    }

    public static void main(String[] args) {
        if (args.length == 1) {
            switch (args[0]) {
                case "publisher":
                    publisher();
                    break;

                case "subscriber":
                    subscriber();
                    break;

                default:
                    usage();
            }
        } else {
            usage();
        }
    }
}
