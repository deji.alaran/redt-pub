package com.redislabs.developertraining;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.StreamEntry;
import redis.clients.jedis.StreamEntryID;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import java.util.concurrent.ThreadLocalRandom;

public class StreamLab {
    private static Jedis jedis = RedisConnection.getConnection();
    private final static String KEY_NAME = "java:streamslab:temperatures";

    /**
     * Helper function to generate a random location ID.
     * @return integer between 1 and 10 inclusive.
     */
    private static int getRandomLocation() {
        return ThreadLocalRandom.current().nextInt(1, 11);
    }

    /**
     * Helper function to generate a random temperature in
     * Fahrenheit.
     * @return an integer between 32 and 90.
     */
    private static int getRandomTemp() {
        return ThreadLocalRandom.current().nextInt(32, 90);
    }

    /**
     * Adds weather observations from a set of remote weather stations
     * to a stream at a fixed rate of one per second.  Stops once 100
     * entries have been added to the stream.
     */
    private static void producer() {
        System.out.println("Starting producer.");
        jedis.del(KEY_NAME);

        // START CODING CHALLENGE

        // Add a new semi randomly generated temperature
        // observation from a group of remote weather stations
        // to the stream at KEY_NAME at a rate of one per second.
        // Stop once 100 entries have been added.
        //
        // When adding new entries to the stream, allow Redis to
        // assign stream entry IDs, and use the following hash like
        // structure as the body for each entry:
        //
        // "location": "1"
        // "tempF": "78"
        //
        // Use the helper methods getRandomLocation() and getRandomTemp()
        // to get sample values for "location" and "tempF".
        //
        // Jedis expects stream entries to be a Map<String, String>
        // with the first string being the key and the second being
        // the value.  This means that you will need to convert the
        // int values for location and tempF to Strings before adding
        // them to a new stream entry.
        //
        // Jedis provides StreamEntryID.NEW_ENTRY to indicate to the
        // xadd method that you want Redis to assign the stream entry
        // an ID automatically.
        //
        // Find example Jedis streams code at:
        // https://github.com/xetorthio/jedis/blob/master/src/test/java/redis/clients/jedis/tests/commands/StreamsCommandsTest.java

        try {
            for (int n = 0; n < 100; n++) {
                Thread.sleep(1000);
                int location = getRandomLocation();
                int tempF = getRandomTemp();
                System.out.println("Adding location: " + location + ", temp " + tempF + " to the stream.");
                Map<String, String> entry = new HashMap<String, String>();
                entry.put("location", String.valueOf(location));
                entry.put("tempF", String.valueOf(tempF));

                // TODO add entry to the stream whose key name is "KEY_NAME",
                // allowing Redis to set the stream entry ID (see Jedis
                // StreamEntryID.NEW_ENTRY)
            }
        } catch (InterruptedException e) {
            System.err.println("Producer interrupted during sleep.");
        }

        // END CODING CHALLENGE

        System.out.println("Producer shutting down.");
    }

    /**
     * Reads entries from a stream, blocking for up to
     * 2 seconds each time it accesses the stream.  If
     * the stream has no new entries 5 consecutive times,
     * the consumer stops trying and returns.
     */
    private static void consumer() {
        System.out.println("Starting consumer.");

        int retryCount = 0;

        StreamEntryID lastEntryId = new StreamEntryID(0, 0);

        while (retryCount < 5) {
            // START CODING CHALLENGE

            // Algorithm:
            //
            // * Perform a blocking read on the stream at KEY_NAME, to get
            //   the one entry from it. Start reading using the supplied value
            //   of lastEntryId which is stream ID 0-0. Remember you will need
            //   to store the ID of the last entry read each time you read
            //   an entry off of the stream, then use that in the next call
            //   to XREAD.
            // * Block for up to 2 seconds or until the stream has a new entry.
            // * If the stream has no new entries (XREAD returns null or
            //   an empty result list):
            //     * Increment retryCount
            // * If the stream has new entries:
            //     * Set retryCount to 0
            //     * Log the location and temperature values read from the stream
            //       (stored in fields "location" and "tempF")
            //
            // Notes:

            // * jedis.xread can read from multiple streams at once, with each stream having
            //   its own name and last seen entry ID.  These need to be wrapped up in a
            //   variable of type Entry<String, StreamEntryID>.  StreamEntryID is a Jedis
            //   class (see declaration of lastEntryId in this class).  You can instantiate
            //   an appropriate variable to pass to jedis.xread as follows:
            //
            //   Entry<String, StreamEntryID> streamQuery =
            //     new AbstractMap.SimpleImmutableEntry<String, StreamEntryID>(
            //       "stream name",
            //       lastEntryId
            //      );
            //
            // * jedis.xread returns List<Map.Entry<String, List<StreamEntry>>>, which is a
            //   list containing one entry per stream read.  The format of that entry is a
            //   map entry, mapping a String containing the stream name to a List of
            //   StreamEntry objects.  Each StreamEntry object then contains a member named
            //   fields, containing a key/value map of the field names/values stored in the
            //   stream entry.  Assuming you store the result of calling jedis.xread in a
            //   variable "results", you can then get the first entry from the first stream
            //   in the list returned as follows:
            //
            //   StreamEntry entry = results.get(0).getValue().get(0);
            //
            //   The map of field names and values contained in the stream entry can then be
            //   obtained with:
            //
            //   entry.getFields()
            //
            //   You want to get the fields named "location" and "tempF" from this map.

            Entry<String, StreamEntryID> streamQuery = new AbstractMap.SimpleImmutableEntry<String, StreamEntryID>(KEY_NAME, lastEntryId);

            // TODO replace the following line with a call to XREAD to 
            // get 1 entry from the stream(s) described in "streamQuery"
            // blocking for 2 seconds and returning results into "results".
            List<Map.Entry<String, List<StreamEntry>>> results = new ArrayList<Map.Entry<String, List<StreamEntry>>>();

            if (results == null || results.size() == 0) {
                System.out.println("Stream has no new entries.");
                retryCount++;
            } else {
                // Get the first entry returned for the first stream read.
                StreamEntry entry = results.get(0).getValue().get(0);
                System.out.println("Read temperature " + entry.getFields().get("tempF") + " from location " + entry.getFields().get("location"));

                // Store the ID of this entry to use in the next XREAD call.
                lastEntryId = entry.getID();

                // Reset the retry count.
                retryCount = 0;
            }

            // END CODING CHALLENGE
        }

        System.out.println("Consumer shutting down.");
    }

    private static void usage() {
        System.err.println("Usage: ListLab producer|consumer");
        System.exit(1);
    }

    public static void main(String[] args) {
        if (args.length == 1) {
            switch (args[0]) {
                case "producer":
                    producer();
                    break;

                case "consumer":
                    consumer();
                    break;

                default:
                    usage();
            }
        } else {
            usage();
        }
    }
}
