package com.redislabs.developertraining;

import redis.clients.jedis.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class HashLab extends LabBase {
    public HashLab(Jedis jedis) {
        super(jedis);
    }

    /**
     * Set the value stored at key to be a hash containing key/value
     * pairs as follows:
     *
     * firstname Jane
     * lastname Doe
     * yearjoined 2008
     * vacationdays 10
     * @param key Redis Key Name.
     */
    public void setHash(String key) {
        Map<String, String> properties = new HashMap<>();

        properties.put("firstname", "Jane");
        // TODO set the other properties.
        // TODO store as a hash at "key".
    }

    /**
     * Gets the hash stored at key, returning it as a Map.
     * @param key Redis key name.
     * @return map containing hash field name/value pairs.
     */
    public Map<String, String> getHash(String key) {
        // TODO
        return new HashMap<>();
    }

    /**
     * Gets the value of an individual field stored in the hash
     * at key, returning it as a String.
     * @param key Redis key name.
     * @param fieldName name of the hash field to get value of.
     * @return the value stored at fieldName
     */
    public String getHashField(String key, String fieldName) {
        // TODO
        return "";
    }

    /**
     * Increments the value of an individual field stored in the
     * hash at key by incrAmount, returning the new value.
     * @param key Redis key name.
     * @param fieldName name of the hash field to increment.
     * @param incrAmount amount to increment the value of fieldName by.
     * @return the new value stored in fieldName.
     */
    public long incrementField(String key, String fieldName, long incrAmount) {
        // TODO
        return -1L;
    }

    /**
     * Gets just the values from the hash stored at key, does
     * not return the field key names.
     * @param key
     * @return List containing field values from the hash.
     */
    public List<String> getHashValues(String key) {
        // TODO
        return new ArrayList<String>();
    }

    /**
     * Adds a new employee, should store:
     *
     * Hash of the employee's properties at key java:test:employees:<id> where
     * <id> is a field in the userProps map.
     *
     * This should also create a secondary index whose keys follow
     * this pattern:
     *
     * java:test:employee:email:<email>
     *
     * where <email> is contained in employeeProps.  This key should be
     * created in a way that its value can be used to retrieved an employee's
     * details when we just know their email address and not their employee ID.
     *
     * @param employeeProps Map containing name/value pairs for the employee's properties.
     */
    public void addUser(Map<String, String> employeeProps) {
        String employeeHashKey = "java:test:employees:" + employeeProps.get("id");
        String employeeByEmailKey = "java:test:employee:email:" + employeeProps.get("email");

        jedis.hmset(employeeHashKey, employeeProps);

        // TODO set the value of employeeByEmailKey to 
        // the employee's "id" property.
    }

    /**
     * Retrieve a large hash using the HSCAN command rather than
     * the HGETALL command.  Retrieve up to 10 items per call to
     * HSCAN, and return the entire hash as a map once it has all
     * been read.
     * @param key
     * @return map containing hash field name/value pairs.
     */
    public Map<String, String> getLargeHash(String key) {
        Map<String, String> hashMap = new HashMap<String, String>();

        ScanParams scanParams = new ScanParams().count(10);
        String cursor = ScanParams.SCAN_POINTER_START;

        do {
            ScanResult<Entry<String, String>> scanResult = jedis.hscan(key, cursor, scanParams);
            List<Entry<String, String>> result = scanResult.getResult();

            for (Entry<String, String> entry: result) {
                // TODO put the entry's key and value into hashMap
            }

            cursor = scanResult.getCursor();
        } while (! cursor.equals(ScanParams.SCAN_POINTER_START));

        return hashMap;
    }
}
