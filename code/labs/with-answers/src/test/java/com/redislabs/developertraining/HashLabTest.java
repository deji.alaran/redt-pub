package com.redislabs.developertraining;

import org.junit.After;
import org.junit.Ignore;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Ignore
public class HashLabTest extends LabTestBase {
    private final HashLab hashLab = new HashLab(jedis);
    private String HASH_KEY;

    public HashLabTest () {
        HASH_KEY = getKeyName("labs:hash");
    }

    @After
    public void afterTest() {
        deleteKeys();
    }

    @Test
    public void setHash() {
        hashLab.setHash(HASH_KEY);

        Map<String, String> retrievedProperties = jedis.hgetAll(HASH_KEY);

        assertThat(retrievedProperties.size() , is(4));
        assertThat(retrievedProperties.get("firstname"), is("Jane"));
        assertThat(retrievedProperties.get("lastname"), is ("Doe"));
        assertThat(retrievedProperties.get("yearjoined"), is("2008"));
        assertThat(retrievedProperties.get("vacationdays"), is("10"));
    }

    @Test
    public void getHash() {
        Map<String, String> properties = createHash();
        Map<String, String> retrievedProperties = hashLab.getHash(HASH_KEY);
        assertThat(retrievedProperties, is(properties));
    }

    @Test
    public void getHashField() {
        Map<String, String> properties = createHash();
        String retrievedValue = hashLab.getHashField(HASH_KEY, "lastname");
        assertThat(retrievedValue, is(properties.get("lastname")));
    }

    @Test
    public void incrementField() {
        Map<String, String> properties = createHash();
        long incrAmount = 2;
        Long expectedValue = new Long(properties.get("vacationdays"));
        expectedValue += incrAmount;

        long retrievedValue = hashLab.incrementField(HASH_KEY, "vacationdays", incrAmount);
        assertThat(retrievedValue, is(expectedValue));
    }

    @Test
    public void getHashValues() {
        Map<String, String> properties = createHash();
        List<String> retrievedVals = hashLab.getHashValues(HASH_KEY);

        assertThat(retrievedVals.size(), is(4));
        assertThat(retrievedVals.contains(properties.get("firstname")), is(true));
        assertThat(retrievedVals.contains(properties.get("lastname")), is(true));
        assertThat(retrievedVals.contains(properties.get("yearjoined")), is(true));
        assertThat(retrievedVals.contains(properties.get("vacationdays")), is(true));
    }

    @Test
    public void addEmployeeTest() {
        Map<String, String> employeeProps = new HashMap<>();
        employeeProps.put("id", "2707");
        employeeProps.put("firstname", "Sandrine");
        employeeProps.put("lastname", "Berger");
        employeeProps.put("yearjoined", "2007");
        employeeProps.put("vacationdays", "12");
        employeeProps.put("email", "sandrine@megacorp.co");

        hashLab.addUser(employeeProps);

        String userByEmailKey = "java:test:employee:email:" + employeeProps.get("email");
        String userId = jedis.get(userByEmailKey);

        assertThat(userId, is(employeeProps.get("id")));

        Map<String, String> userHash = jedis.hgetAll("java:test:employees:" + userId);
        assertThat(userHash, is(employeeProps));
    }

    @Test
    public void getLargeHash() {
        Map<String, String> hashProps = new HashMap<>();
        for (int n = 0; n < 2000; n++) {
            String fieldName = "field" + String.valueOf(n);
            String value = "value" + String.valueOf(n);
            hashProps.put(fieldName, value);
        }

        jedis.hmset(HASH_KEY, hashProps);

        Map<String, String> largeHash = hashLab.getLargeHash(HASH_KEY);

        assertThat(largeHash, is(hashProps));
    }

    private Map<String, String> createHash() {
        Map<String, String> properties = new HashMap<>();
        properties.put("firstname", "Miguel");
        properties.put("lastname", "Martinez");
        properties.put("yearjoined", "2016");
        properties.put("vacationdays", "10");
        jedis.hmset(HASH_KEY, properties);
        return properties;
    }
}
