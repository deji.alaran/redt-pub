package com.redislabs.developertraining;

import org.junit.After;
import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;

@Ignore
public class StringLabTest extends LabTestBase {
    private final StringLab stringLab = new StringLab(jedis);
    private String TASK_ONE_KEY;
    private String TASK_TWO_KEY;

    public StringLabTest() {
        TASK_ONE_KEY = getKeyName("labs:string:task1");
        TASK_TWO_KEY = getKeyName("labs:string:task2");
    }

    @After
    public void afterTest() {
        deleteKeys();
    }

    @Test
    public void setString() {
        stringLab.setString(TASK_ONE_KEY, "Hello World");
        String str = jedis.get(TASK_ONE_KEY);
        assertThat(str, is("Hello World"));
    }

    @Test
    public void getString() {
        String strValue = "Hello World";
        jedis.set(TASK_ONE_KEY, strValue);
        String str = stringLab.getString(TASK_ONE_KEY);
        assertThat(str, is(strValue));
    }

    @Test
    public void setExpiry() {
        jedis.set(TASK_ONE_KEY, "Hello World");
        stringLab.setExpiry(TASK_ONE_KEY, 60);
        Long ttl = jedis.ttl(TASK_ONE_KEY);
        assertThat(ttl, greaterThan(new Long(-1)));
    }

    @Test
    public void getTimeToLive() {
        jedis.set(TASK_ONE_KEY, "Hello World");
        Long ttl = stringLab.getTimeToLive(TASK_ONE_KEY);
        assertThat(ttl, greaterThan(new Long(-2)));
    }

    @Test
    public void getStringRange() {
        jedis.set(TASK_TWO_KEY, "Hello Redis");
        String subString = stringLab.getStringRange(TASK_TWO_KEY);
        assertThat(subString, is("Redis"));
    }
}
