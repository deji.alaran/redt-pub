package com.redislabs.developertraining;

public class LandmarkWithDistance extends Landmark {
    private double distance;

    public LandmarkWithDistance(String name, double longitude, double latitude, double distance) {
        super(name, longitude, latitude);
        this.distance = distance;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }
}
