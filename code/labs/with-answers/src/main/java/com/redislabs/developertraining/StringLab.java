package com.redislabs.developertraining;

import redis.clients.jedis.Jedis;

public class StringLab extends LabBase {
    public StringLab(Jedis jedis) {
        super(jedis);
    }

    /**
     * Sets the value stored at key to be value.
     * @param key Redis key.
     * @param value Value to be stored.
     */
    public void setString(String key, String value) {
        jedis.set(key, value);
    }

    /**
     * Gets the value stored at key.
     * @param key Redis key.
     * @return The value stored at key.
     */
    public String getString(String key) {
        return jedis.get(key);
    }

    /**
     * Sets an expiry time for a key.
     * @param key Redis key.
     * @param expiry Time in seconds before the key expires.
     */
    public void setExpiry(String key, int expiry) {
        jedis.expire(key, expiry);
    }

    /**
     * Gets the remaining time to live for a key.
     * @param key Redis key.
     * @return
     */
    public Long getTimeToLive(String key) {
        return jedis.ttl(key);
    }

    /**
     * Returns the last 5 characters from the string
     * value stored at key.
     * @param key Redis key.
     * @return String containing last 5 characters from the value at key.
     */
    public String getStringRange(String key) {
        return jedis.getrange(key, -5, -1);
    }
}
