package com.redislabs.developertraining;

import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SetLab extends LabBase {
    public SetLab(Jedis jedis) {
        super(jedis);
    }

    /**
     * Return the number of members that appear in both the set
     * stored at set1Key and the set stored at set2Key.
     * @param set1Key Redis key name.
     * @param set2Key Redis key name.
     * @return the number of members that appear in both sets.
     */
    public int getIntersectingCount(String set1Key, String set2Key) {
        Set<String> intersectingMembers = jedis.sinter(set1Key, set2Key);
        return intersectingMembers.size();
    }

    /**
     * Calculate the difference between two sets, storing the
     * result in a third set.
     * @param set1Key Redis key holding the first set.
     * @param set2Key Redis key holding the second set.
     * @param resultSetKey Redis key where the set containing difference
     *                     of set1Key and set2Key is to be stored.
     */
    public void getSetDifferenceAndStore(String set1Key, String set2Key, String resultSetKey) {
        // A naive implementation might run the diff on set1Key and set2Key
        // then loop over the results adding values to resultSetKey.
        jedis.sdiffstore(resultSetKey, set1Key, set2Key);
    }

    /**
     * This method is similar to one we saw in the Hash lab, so part of it
     * has been implemented for you.  You will need to store the employee's
     * tags in Redis.
     * @param key Redis key to use when writing the employee hash.
     * @param employeeProperties Map of properties representing the employee hash.
     */
    public void addUserWithTags(String key, Map<String, String> employeeProperties) {
        // Write the employee hash to Redis.
        jedis.hmset(key, employeeProperties);

        // To complete this method, you need to use the following
        // entries from the employeeProperties map:
        //
        // * id - the employee's ID.
        // * tags - a comma separated list of tags.
        //
        // For each tag, write the employee's ID to a set whose key is:
        //
        // java:test:employeetags:<tag>

        String[] tags = employeeProperties.get("tags").split(",");
        String employeeId = employeeProperties.get("id");

        // Pipeline this if we have covered that at this point?
        for (int n = 0; n < tags.length; n++) {
            String setKey = "java:test:employeetags:" + tags[n];
            jedis.sadd(setKey, employeeId);
        }
    }

    /**
     * Get the set of employee IDs that have all of the tags
     * in the tags array.
     *
     * To implement this method successfully, you must turn
     * each tag into its Redis key name which is:
     *
     * java:test:employeetags:<tag>
     *
     * @param tags array of Strings, each string is a tag.
     * @return set of employee IDs that have all of the tags in the tags array.
     */
    public Set<String> getEmployeeIdsWithTags(String ...tags) {
        List<String> tagKeyNames = new ArrayList<String>();

        for (int n = 0; n < tags.length; n++) {
            tagKeyNames.add("java:test:employeetags:" + tags[n]);
        }

        return jedis.sinter(tagKeyNames.toArray(new String[0]));
    }
}
