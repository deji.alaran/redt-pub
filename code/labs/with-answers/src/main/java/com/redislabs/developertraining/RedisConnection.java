package com.redislabs.developertraining;

import redis.clients.jedis.Jedis;

public class RedisConnection {
    private final static String REDIS_HOST = "redis";
    private final static int REDIS_PORT = 6379;
    private final static String REDIS_PASSWORD = "";

    public static Jedis getConnection() {
        Jedis jedis = new Jedis(REDIS_HOST, REDIS_PORT);

        if (REDIS_PASSWORD.length() > 0) {
            jedis.auth(REDIS_PASSWORD);
        }

        return jedis;
    }
}
