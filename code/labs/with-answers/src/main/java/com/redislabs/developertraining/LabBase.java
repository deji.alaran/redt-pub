package com.redislabs.developertraining;

import redis.clients.jedis.Jedis;

public abstract class LabBase {
    protected final Jedis jedis;

    public LabBase(Jedis jedis) {
        this.jedis = jedis;
    }
}
