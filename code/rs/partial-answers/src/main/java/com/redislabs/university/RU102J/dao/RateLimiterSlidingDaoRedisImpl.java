package com.redislabs.university.RU102J.dao;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Response;
import redis.clients.jedis.Transaction;

import java.time.ZonedDateTime;
import com.redislabs.university.RU102J.core.KeyHelper;

public class RateLimiterSlidingDaoRedisImpl implements RateLimiter {

    private final JedisPool jedisPool;
    private final long windowSizeMS;
    private final long maxHits;

    public RateLimiterSlidingDaoRedisImpl(JedisPool pool, long windowSizeMS,
                                          long maxHits) {
        this.jedisPool = pool;
        this.windowSizeMS = windowSizeMS;
        this.maxHits = maxHits;
    }

    // Challenge #7
    @Override
    public void hit(String name) throws RateLimitExceededException {
        // START CHALLENGE #7
        try (Jedis jedis = jedisPool.getResource()) {
            String key = KeyHelper.getKey("limiter:" + windowSizeMS + ":" + name + ":" + maxHits);
            long now = ZonedDateTime.now().toInstant().toEpochMilli();

            Transaction t = jedis.multi();
            String member = now + "-" + Math.random();

            // TODO Add a ZADD command to the transaction that adds the value "member" to the
            // sorted set whose key is "key".  Set the score for "member" to be the value
            // of "now" (time in milliseconds).

            // TODO Add a ZREMRANGEBYSCORE command to the transaction that removes all members
            // of the sorted set whose key is "key", and whose scores are between 0 and
            // now - windowSizeMS (older than the current sliding window).

            // TODO Add a ZCARD command to the transaction to get the cardinality of the
            // sorted set whose key is "key".  Store that value in a variable named
            // "hits".

            t.exec();

            // TODO Add logic here to throw a new RateLimitExceededException if the value
            // of "hits" is greater than the value of "maxHits".
        }
        // END CHALLENGE #7
    }
}
