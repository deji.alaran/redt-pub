package com.redislabs.lettuce;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import io.lettuce.core.LettuceFutures;
import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisFuture;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.async.RedisAsyncCommands;
import io.lettuce.core.api.sync.RedisCommands;
import io.netty.util.internal.shaded.org.jctools.queues.MessagePassingQueue.Supplier;

public class Runner {
    private RedisClient redisClient = RedisClient.create("redis://@localhost:6379/");
    private StatefulRedisConnection<String, String> connection = redisClient.connect();
    private RedisCommands<String, String> syncCommands = connection.sync();
    private RedisAsyncCommands<String, String> asyncCommands = connection.async();

    private int totalRecords = 100000;
    private final int recordsPerBatch = 100;
    private int totalBatchCount = totalRecords / recordsPerBatch;
    private List<Integer> numbers;
    private Long sum;

    public Runner(int totalRecords) {
        this.totalRecords = totalRecords;
        totalBatchCount = totalRecords / recordsPerBatch;
        numbers = IntStream.rangeClosed(1, totalRecords).boxed().collect(Collectors.toList());
        sum = numbers.stream().mapToLong(i -> i).sum();
    }

    public Supplier<List<Integer>> groupingBy() {
        Map<Integer, List<Integer>> groups = numbers.stream().collect(Collectors.groupingBy(s -> s % totalBatchCount));
        final List<List<Integer>> subSets = new ArrayList<List<Integer>>(groups.values());
        final AtomicInteger counter = new AtomicInteger(0);

        return () -> {
            int next = counter.getAndIncrement();
            if (next < subSets.size()) {
                return subSets.get(next);
            }
            return null;
        };
    }

    public Supplier<List<Integer>> subList() {
        final AtomicInteger counter = new AtomicInteger(0);

        return () -> {
            int next = counter.getAndIncrement();
            int start = next * recordsPerBatch;
            int end = ((next + 1) * recordsPerBatch) - 1;
            if (start >= numbers.size())
                return null;
            if (end >= numbers.size())
                end = numbers.size() - 1;

            return numbers.subList(start, end);
        };
    }

    public void sync(Supplier<List<Integer>> data) {
        List<Integer> dataToRun = data.get();
        while (dataToRun != null) {
            for (Integer i : dataToRun) {
                syncCommands.multi();
                syncCommands.set("data:" + i, String.valueOf(i));
                syncCommands.incrby("sumofallcounters", i);
                syncCommands.exec();
            }
            dataToRun = data.get();
        }
    }

    public void pipelinedSingleMulti(Supplier<List<Integer>> data) {
        asyncCommands.setAutoFlushCommands(false);
        List<Integer> dataToRun = data.get();
        while (dataToRun != null) {
            List<RedisFuture<?>> futures = new ArrayList<>();
            asyncCommands.multi();
            for (Integer i : dataToRun) {
                futures.add(asyncCommands.set("data:" + i, String.valueOf(i)));
                futures.add(asyncCommands.incrby("sumofallcounters", i));
            }
            asyncCommands.exec();
            asyncCommands.flushCommands();
            LettuceFutures.awaitAll(1, TimeUnit.MINUTES, futures.toArray(new RedisFuture[futures.size()]));
            dataToRun = data.get();
        }
        asyncCommands.setAutoFlushCommands(true);
    }

    public void pipelinedLoopedMulti(Supplier<List<Integer>> data) {
        asyncCommands.setAutoFlushCommands(false);
        List<Integer> dataToRun = data.get();
        while (dataToRun != null) {
            List<RedisFuture<?>> futures = new ArrayList<>();
            for (Integer i : dataToRun) {
                asyncCommands.multi();
                futures.add(asyncCommands.set("data:" + i, String.valueOf(i)));
                futures.add(asyncCommands.incrby("sumofallcounters", i));
                asyncCommands.exec();
            }
            asyncCommands.flushCommands();
            LettuceFutures.awaitAll(1, TimeUnit.MINUTES, futures.toArray(new RedisFuture[futures.size()]));
            dataToRun = data.get();
        }
        asyncCommands.setAutoFlushCommands(true);
    }

    public void pipelinedNoMulti(Supplier<List<Integer>> data) {
        asyncCommands.setAutoFlushCommands(false);
        List<Integer> dataToRun = data.get();
        while (dataToRun != null) {
            List<RedisFuture<?>> futures = new ArrayList<>();
            for (Integer i : dataToRun) {
                futures.add(asyncCommands.set("data:" + i, String.valueOf(i)));
                futures.add(asyncCommands.incrby("sumofallcounters", i));
            }
            asyncCommands.flushCommands();
            LettuceFutures.awaitAll(1, TimeUnit.MINUTES, futures.toArray(new RedisFuture[futures.size()]));
            dataToRun = data.get();
        }
        asyncCommands.setAutoFlushCommands(true);
    }

    public void clear() {
        syncCommands.flushall();
    }

    public void assertSum(long expectedValue) {
        assert (syncCommands.get("sumofallcounters") == String.valueOf(expectedValue));
    }

    public void doRun(Consumer<?> s) {
        clear();
        assertSum(0);
        long start = System.currentTimeMillis();
        s.accept(null);
        long end = System.currentTimeMillis();
        System.out.println("Took " + (end - start) + "ms");
        assertSum(sum);
    }

    public void doPartitioningRun(Supplier<List<Integer>> s) {
        long start = System.nanoTime();
        List<Integer> list;
        long localSum = 0;
        while ((list = s.get()) != null) {
            localSum += list.stream().mapToInt(i -> i).sum();
        }
        long end = System.nanoTime();
        System.out.println("Took " + (end - start) + "ns");

        assert (sum == localSum);
    }

    public static void runPartitioningTests(Runner r) {
        r.doPartitioningRun(r.groupingBy());
        r.doPartitioningRun(r.subList());
    }

    public static void runConnectionTests(Runner r) {
        r.doRun((a) -> r.sync(r.groupingBy()));
        r.doRun((a) -> r.pipelinedSingleMulti(r.groupingBy()));
        r.doRun((a) -> r.pipelinedLoopedMulti(r.groupingBy()));
        r.doRun((a) -> r.pipelinedNoMulti(r.groupingBy()));
    }

    public static void main(String args[]) {

        List<Integer> runs = Arrays.asList(1000, 10000, 100000, 1000000, 5000000, 10000000);

        for (int i = 0; i < 5; i++) {
            runs.stream().forEach((totalRecs) -> {
                System.out.println("Running " + totalRecs);

                Runner r = new Runner(totalRecs);
                runPartitioningTests(r);
                // runConnectionTests(r);
            });
        }
    }
}