# Developer Training Lab Environment

Create the environment used for developer training

## Pre-requisites

[Docker](https://docs.docker.com/engine/install/) and [Docker-Compose](https://docs.docker.com/compose/install/)

## How to run

- Clone this repository
- docker-compose up -d (to start).  *Docker hangs sometimes while downloading so Ctrl^C and rerun command)*
- docker-compose down --remove-orphans (to stop)
- go to http://localhost in your browser

*This container is not optimized*
  
Alternatively, you can setup your own Redis instance and use your own IDE to run the code.  The source is under the code/ directory

## Topology

![Technical Seminars](docs/img/redt-topology.png)

### Accessible Components

IDE: http://localhost.  Password is trainee  
Redis Insight: http://localhost:8001.  Redis host is redis and port is 6379  
RediSolar: http://localhost/redisolar (when running RediSolar server)  
