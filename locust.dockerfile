FROM locustio/locust:1.0.1

COPY scripts/requirements.txt requirements.txt

RUN pip install -r requirements.txt
