FROM ubuntu

ARG GCLOUD_SERVICE_KEY

RUN apt-get update && \
    apt-get install -y docker && \
    apt-get install -y docker-compose && \
    apt-get install -y gnupg2 pass

RUN echo "$GCLOUD_SERVICE_KEY" > gcloud-service-key.json && \
     docker login -u _json_key --password-stdin https://us.gcr.io < gcloud-service-key.json && \
     rm gcloud-service-key.json

COPY code code
COPY postgres postgres
COPY scripts scripts
COPY docker-compose.yml docker-compose.yml
COPY nginx.conf nginx.conf
COPY prometheus prometheus
COPY alertmanager alertmanager
COPY locust.yml locust.yml

RUN chown 1000:1000 code && \
    mkdir -p 'postgres/data/./pg_commit_ts' && \
    mkdir -p 'postgres/data/./pg_dynshmem' && \
    mkdir -p 'postgres/data/./pg_logical' && \
    mkdir -p 'postgres/data/./pg_logical/mappings' && \
    mkdir -p 'postgres/data/./pg_logical/snapshots' && \
    mkdir -p 'postgres/data/./pg_multixact' && \
    mkdir -p 'postgres/data/./pg_multixact/members' && \
    mkdir -p 'postgres/data/./pg_multixact/offsets' && \
    mkdir -p 'postgres/data/./pg_notify' && \
    mkdir -p 'postgres/data/./pg_replslot' && \
    mkdir -p 'postgres/data/./pg_serial' && \
    mkdir -p 'postgres/data/./pg_snapshots' && \
    mkdir -p 'postgres/data/./pg_stat' && \
    mkdir -p 'postgres/data/./pg_stat_tmp' && \
    mkdir -p 'postgres/data/./pg_subtrans' && \
    mkdir -p 'postgres/data/./pg_tblspc' && \
    mkdir -p 'postgres/data/./pg_twophase' && \
    mkdir -p 'postgres/data/./pg_wal' && \
    mkdir -p 'postgres/data/./pg_wal/archive_status' && \
    mkdir -p 'postgres/data/./pg_xact'

CMD (dockerd &) && sleep 5 && docker-compose up